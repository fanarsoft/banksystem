﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Resources
{
    public class TransactionResorce
    {
        public int Id { get; set; }
        public int CurrentId { get; set; }
        public int TargetId { get; set; }
        public int Amount { get; set; }
        public int TransactionPrice { get; set; }
        public DateTime TransactionData { get; set; }
    }
}
