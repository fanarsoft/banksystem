﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BankSystem.Models;
using BankSystem.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BankSystem.Controllers
{
    [Route("/api/bank")]
    public class BankSystemController : Controller
    {
        BankServies Bank = BankServies.getInstance();
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [Authorize]
        public IActionResult CreateAccount([FromBody] int accountId, string maincurrancy)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            Bank.CreateAccount(accountId, maincurrancy);
            return Ok();
        }
        [HttpDelete("{id}")]
        [Authorize]
        public IActionResult DeleteAccount(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            Bank.DeleteAccount(id);
            return Ok();
        }
        [HttpGet("{id}")]
        public IActionResult GetAccountById(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var account = Bank.GetAccountById(id);
            return Ok(account);
        }
        [HttpGet]
        public IActionResult GetAccountList()
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var accounts = Bank.GetAccountList();
            return Ok(accounts);
        }
        [HttpGet("{id}")]
        public IActionResult GetMainCurrancyForAccount(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var type = Bank.GetMainCurrancyForAccount(id);
            return Ok(type);
        }
        [HttpGet("{id}")]
        public IActionResult GetListOfAccountType(int id)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            var accountTypes = Bank.GetListOfAccountType(id);
            return Ok(accountTypes);
        }
        [HttpPost("{id}")]
        public IActionResult SetMaincurrancyForAccount(int id, string currancy)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            Bank.SetMaincurrancyForAccount(id, currancy);
            return Ok();
        }
        [HttpPost]
        public IActionResult AddAccuntType(AccountType accountType)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            Bank.AddAccuntType(accountType);
            return Ok();
        }
        [HttpPost("{id}")]
        public IActionResult Withdraw(int id,int amount,string currancyType)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            Bank.Withdraw(id,amount, currancyType);
            return Ok();
        }
        [HttpPost("{id}")]
        public IActionResult Deposit(int id, int amount, string currancyType)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            Bank.Deposit(id, amount, currancyType);
            return Ok();
        }
        [HttpPost("{id}")]
        public IActionResult Transfer(int currantId,int targetId, int amount)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            Bank.Transfer(currantId, targetId, amount);
            return Ok();
        }
    }
}
