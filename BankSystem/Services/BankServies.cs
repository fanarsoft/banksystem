﻿using BankSystem.Data;
using BankSystem.Interfaces;
using BankSystem.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Services
{
    public class BankServies : IBank
    {
        private static BankServies BankInstance = new BankServies();
        private readonly BankContext context = new BankContext();
        private Bank Bank = new Bank();
        private AccountTypeServies AccountTypeServies = new AccountTypeServies();
        private AccountService AccountService = new AccountService();
        private BankServies() { }

        public static BankServies getInstance()
        {
            if(BankInstance==null)
            {
                BankInstance = new BankServies();
            }
            return BankInstance;
        }

        public BankServies(BankContext context, Bank bank, AccountTypeServies accountTypeServies, AccountService accountService)
        {
            this.context = context;
            Bank = bank;
            AccountTypeServies = accountTypeServies;
            AccountService = accountService;
        }

        public void SetBankName(string name)
        {
            Bank.Name = name;
        }

        public void CreateAccount(int accountId, string maincurrancy)
        {
            AccountService.CreatAccount(maincurrancy, accountId);
        }

        public void AddAccountToDatabase(Account account)
        {
            AccountService.AddAcountToDb(account);
        }

        public void DeleteAccount(int id)
        {
            AccountService.DeleteAccount(id);
        }

        public Account GetAccountById(int id)
        {
            return AccountService.GetAcCountById(id);
        }

        public List<Account> GetAccountList()
        {
            return AccountService.GetAcountList();
        }

        public List<AccountType> GetListOfAccountType(int id)
        {
            return AccountService.GetAcountTypeList(id);
        }

        public void SetMaincurrancyForAccount(int id,string mainCurrancy)
        {
            AccountService.SetMainCurrancy(id,mainCurrancy);
        }

        public string GetMainCurrancyForAccount(int id)
        {
            return AccountService.GetMainCurrancy(id);
        }

        public void CreateAccountType(int AccountId,int Amount, string Currany, string Type)
        {
            AccountTypeServies.CreateAccountType(AccountId, Currany, Amount, Type);
        }

        public void Deposit(int AccountTypeId,double Amount,string currancyType)
        {
            AccountTypeServies.Deposit(AccountTypeId, Amount, currancyType);
        }

        public void AddAccuntType(AccountType accountType)
        {
            AccountTypeServies.AddAccountTypeToDatabase(accountType);
        }

        public void DeleteAccountType(int id)
        {
            AccountTypeServies.DeleteAccountType(id);
        }

        public double GetAccountTypeAmount(int id)
        {
            return AccountTypeServies.GetAccountAmount(id);
        }

        public void DeleteAccounts(int id)
        {
            AccountService.DeleteAccount(id);
        }

        public void Withdraw(int id,double amout,string currancyType)
        {
            AccountTypeServies.Withdraw(id,amout,currancyType);
        }

        public void Transfer(int currantId,int tagetId,int amount)
        {
            AccountTypeServies.Transfer(currantId, tagetId, amount);

        }

    }
}
