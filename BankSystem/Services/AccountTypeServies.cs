﻿using BankSystem.Data;
using BankSystem.Interfaces;
using BankSystem.Models;
using System.Collections.Generic;
using System.Linq;
using static BankSystem.Services.TransactionServies;

namespace BankSystem.Services
{
    public class AccountTypeServies : IAccountType
    {
        BankContext context = new BankContext();
        AccountType accountType = new AccountType();
        AccountType accountTypeTarget = new AccountType();
        TransactionDelegate CreateTransactionLog = TransactionServies.CreateTransaction;

        public void AddAccountTypeToDatabase(AccountType accountType)
        {
            context.AccountTypes.Add(accountType);
        }

        public AccountType GetAccountTypeById(int id)
        {
            AccountType accountType = context.AccountTypes.SingleOrDefault(x => x.Id == id);
            return accountType;

        }

        public static double ConvertCurruncy(double amount, string sourceCurruncy, string targetCurruncy)
        {
            var CurruncyRates = new Dictionary<string, Dictionary<string, double>>
            {
                { "usd", new Dictionary<string, double> { { "usd", 1 }, { "nis", 3.5 }, { "jod", 0.80 } } },
                { "nis", new Dictionary<string, double> { { "usd", 0.3 }, { "nis", 1 }, { "jod", 0.2 } } },
                { "jod", new Dictionary<string, double> { { "usd", 1.2 }, { "nis", 5 }, { "jod", 1 } } },
            };
            return amount * CurruncyRates[sourceCurruncy][targetCurruncy];
        }

        public AccountType CreateAccountType(int accountId, string currancyType, int amount, string type)
        {
            accountType.AccountId = accountId;
            accountType.CurrancyType = currancyType;
            accountType.Amount = amount;
            accountType.Type = type;
            return accountType;
        }

        public void DeleteAccountType(int id)
        {
            AccountType accountType = context.AccountTypes.SingleOrDefault(x => x.Id ==id);
            context.AccountTypes.Remove(accountType);
            context.SaveChanges();
        }

        public void Deposit(int id,double amount,string currancyType)
        {
            AccountType accountType = context.AccountTypes.SingleOrDefault(x => x.Id == id);
            double convertedAmount = ConvertCurruncy(amount, currancyType, accountType.CurrancyType);
            accountType.Amount = accountType.Amount + convertedAmount;
            context.AccountTypes.Update(accountType);
            CreateTransactionLog(accountType.Id, accountType.Id, amount, "Deposit");

        }

        public double GetAccountAmount(int id)
        {
            AccountType accountType = GetAccountTypeById(id);
            return accountType.Amount;
        }

        public string GetAccountType(int id)
        {
            AccountType accountType = GetAccountTypeById(id);
            return accountType.Type;
        }

        public string GetCurrancyType(int id)
        {
            AccountType accountType = GetAccountTypeById(id);
            return accountType.CurrancyType;
        }

        public void SetAccountAmount(int id,int amount)
        {
            AccountType accountType = GetAccountTypeById(id);
            accountType.Amount = amount;
        }

        public void SetAccountType(int id,string Type)
        {
            AccountType accountType = GetAccountTypeById(id);
            accountType.Type = Type;
        }

        public void SetCurrancyType(int id,string type)
        {
            AccountType accountType = GetAccountTypeById(id);
            accountType.CurrancyType = type;
        }

        public void Transfer(int sourceId, int tagetId, double amount)
        {
            AccountType suorceAccount =GetAccountTypeById(sourceId);
            accountType.Amount = accountType.Amount - amount;
            AccountType TargetAccountType = GetAccountTypeById(tagetId);
            double convertedAomunt = ConvertCurruncy(amount, suorceAccount.CurrancyType, TargetAccountType.CurrancyType);
            TargetAccountType.Amount = TargetAccountType.Amount + convertedAomunt;
            context.AccountTypes.Update(accountType);
            context.AccountTypes.Update(TargetAccountType);
            CreateTransactionLog(accountType.Id, tagetId, convertedAomunt, "Transfer");

        }

        public void Withdraw(int id,double amount,string currancyType)
        {
            AccountType accountType = GetAccountTypeById(id);
            double convertedAmount = ConvertCurruncy(amount, accountType.CurrancyType, currancyType);
            accountType.Amount = accountType.Amount - convertedAmount;
            context.AccountTypes.Update(accountType);
            CreateTransactionLog(accountType.Id, accountType.Id, amount, "Withdraw");
        }
    }
}
