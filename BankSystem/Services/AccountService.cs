﻿using BankSystem.Data;
using BankSystem.Interfaces;
using BankSystem.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BankSystem.Services
{
    public class AccountService : IAccount
    {
        private BankContext context = new BankContext();
        private Account account = new Account();
        static Random random = new Random();

        public static int GenerateRandom()
        {
            List<int> randomNumbers = new List<int>();
            int number;
            do number = random.Next();
            while (randomNumbers.Contains(number));
            randomNumbers.Add(number);
            return number;
        }

        public void AddAcountToDb(Account account)
        {
            context.Accounts.Add(account);
            context.SaveChanges();
        }

        public Account CreatAccount(string currancy, int accountID)
        {
            account.MainCurrency = currancy;
            account.AccountID = accountID;
            return account;
        }

        public void DeleteAccount(int id)
        {
            Account account = GetAcCountById(id);
            context.Accounts.Remove(account);
            context.SaveChanges();
        }

        public Account GetAcCountById(int id)
        {
            Account account = context.Accounts.SingleOrDefault(x => x.Id == id);
            return account;
        }

        public List<Account> GetAcountList()
        {
            var Accounts = context.Accounts.Include(m => m.AccountTypes);
            return (List<Account>)Accounts;
        }

        public List<AccountType> GetAcountTypeList(int id)
        {
            Account account = GetAcCountById(id);
            return account.AccountTypes.ToList();
        }

        public void SetMainCurrancy(int id,string mainCurrancy)
        {
            Account account = GetAcCountById(id);
            account.MainCurrency = mainCurrancy;
        }

        public string GetMainCurrancy(int id)
        {
            Account account = GetAcCountById(id);
            return account.MainCurrency;
        }

        public void UpdataAccount(int id)
        {
            Account account = GetAcCountById(id);
            context.Accounts.Update(account);
        }
    }
}
