﻿using BankSystem.Data;
using BankSystem.Interfaces;
using BankSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace BankSystem.Services
{
    public class TransactionServies 
    {

        private readonly static BankContext context = new BankContext();
        private static TransactionLog transactionLog = new TransactionLog();
        public  delegate void TransactionDelegate (int currentId, int targetId, double amount, string type);
        public static void CreateTransaction(int currentId, int targetId, double amount, string type)
        {
            transactionLog.SourceAcountTypeId = currentId;
            transactionLog.TargetAcountTypeId = targetId;
            transactionLog.Amount = amount;
            transactionLog.TransactionType = type;
            transactionLog.TransactionDate = DateTime.Now;
            context.TransactionsLog.Add(transactionLog);
            context.SaveChanges();
        }

        public static string GetTransactionDetailes(int id)
        {
            TransactionLog transactionLog = context.TransactionsLog.SingleOrDefault(x => x.Id == id);
            return transactionLog.TransactionType + transactionLog.TransactionDate.ToString() +
                transactionLog.TargetAcountTypeAmountAfter.ToString() + transactionLog.TargetAcountTypeAmountBefore.ToString() +
                transactionLog.SourceAcountTypeAmountAfter.ToString() + transactionLog.SourceAcountTypeAmountBefore.ToString() +
                transactionLog.TargetAcountTypeId.ToString();
        }
    }
}
