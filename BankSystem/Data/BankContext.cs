﻿using BankSystem.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Data
{
    public class BankContext : DbContext
    {
        public DbSet<Bank> Banks { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<TransactionLog> TransactionsLog { get; set; }
        public DbSet<AccountType> AccountTypes { get; set; }


        public BankContext()
        {
        }
    }   
}
