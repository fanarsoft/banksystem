import { Component, Inject, OnInit } from '@angular/core';
import { AccountService } from 'src/service/account.service';
import { Router } from '@angular/router';
import { AccountTypeService } from 'src/service/account-type.service';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './account-list.component.html'
})
export class AccountList implements OnInit {
  accounts;
  constructor(private acountService :AccountService,private router: Router,private accountTypes :AccountTypeService)
  {
  }
  ngOnInit() {
    this.acountService.getAccountList()
    .subscribe(accounts=>this.accounts=accounts)
  }
  onClick(id:number)
  {
    this.router.navigateByUrl('/account/detail/${id}');
  }
}


