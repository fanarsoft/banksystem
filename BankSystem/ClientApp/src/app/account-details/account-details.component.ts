import { Component, Inject, OnInit } from '@angular/core';
import { AccountService } from 'src/service/account.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fetch-data',
  templateUrl: './account-details.component.html'
})
export class AccountDetail implements OnInit {
  accounts;
  constructor(private acountService :AccountService,private router: Router)
  {
  }
  ngOnInit() {
    this.acountService.getAccountList()
    .subscribe(accounts=>this.accounts=accounts)
  }
  onClick(id)
  {
    this.router.navigateByUrl('/account/detail',id);
  }
}


