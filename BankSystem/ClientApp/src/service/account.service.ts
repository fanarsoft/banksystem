import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable()
export class AccountService{
    constructor(private http:HttpClient){ }
    getAccountList()
    {
        return this.http.get('/api/bank/GetAccountList')
          
    }
}
