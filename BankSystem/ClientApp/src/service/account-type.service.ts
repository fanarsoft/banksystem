import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable()
export class AccountTypeService{
    constructor(private http:HttpClient){ }
    getAcountType(id)
    {
        return this.http.get('/api/bank/GetAcountType/${id}')
          
    }
}
