﻿using AutoMapper;
using BankSystem.Models;
using BankSystem.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Mapping
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<Account, AccountResorce>();
            CreateMap<AccountType, AccountTypeResorce>();
            CreateMap<Bank, BankResorce>();
            CreateMap<TransactionLog, TransactionResorce>();
        }
    }
}
