﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Models
{
    public class Account
    {
        public int Id { get; set; }
        [Required]
        [StringLength(255)]
        public string MainCurrency { get; set; }
        [Required]
        public string Type { get; set; }
        public int AccountID { get; set; }
        public string name { get; set; }
        public List<AccountType> AccountTypes { get; set; }

        public Account()
        {
            AccountTypes = new List<AccountType>();
        }
        public override bool Equals(object value)
        {
            return (value is Account account)
                && (Id == account.Id)
                && (MainCurrency == account.MainCurrency)
                && (Type == account.Type)
                && (name == account.name);
        }
    }
}
