﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Models
{
    public class AccountType
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int AccountId { get; set; }
        public string CurrancyType { get; set; }
        public double Amount { get; set; }
        public ICollection<TransactionLog> Transactions { get; set; }

        public AccountType()
        {
            Transactions = new Collection<TransactionLog>();
        }
        public override bool Equals(object value)
        {
            return (value is AccountType account)
                && (Id == account.Id)
                && (CurrancyType == account.CurrancyType)
                && (Type == account.Type)
                && (Amount == account.Amount);
        }

    }
}
