﻿using System;
namespace BankSystem.Models
{
    public class TransactionLog
    {
        public int Id { get; set; }
        public int SourceAcountTypeId { get; set; }
        public int TargetAcountTypeId { get; set; }
        public int TargetAcountTypeAmountAfter { get; set; }
        public int SourceAcountTypeAmountAfter { get; set; }
        public int TargetAcountTypeAmountBefore { get; set; }
        public int SourceAcountTypeAmountBefore { get; set; }
        public double Amount { get; set; }
        public string TransactionType { get; set; }
        public int TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public int AccountTypeId { get; set; }
    }
}

