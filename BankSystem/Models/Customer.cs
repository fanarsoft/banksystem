namespace BankSystem.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public int AccountID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string phone { get; set; }
    }
}