﻿using BankSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Interfaces
{
    interface IAccountType
    { 
        string GetAccountType(int id);
        AccountType GetAccountTypeById(int id);
        AccountType CreateAccountType(int accountId,string currancyType, int amount,string type);
        string GetCurrancyType(int id);
        void SetAccountType(int id,string Type);
        void SetCurrancyType(int id,string type);
        double GetAccountAmount(int id);
        void SetAccountAmount(int id,int amount);
        void Withdraw(int id,double amount,string currancyType);
        void Deposit(int id,double amount,string currancyType);
        void Transfer(int sourceId,int tagetId,double amount);
        void AddAccountTypeToDatabase(AccountType accountType);
        void DeleteAccountType(int id);
    }
}
