﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Interfaces
{
    interface IBank
    {
        void SetBankName(string name);
    }
}
