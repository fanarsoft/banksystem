﻿using BankSystem.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankSystem.Interfaces
{
   public interface IAccount
    {
        List<Account> GetAcountList();
        Account GetAcCountById(int id);
        Account CreatAccount(string currancy, int accountID);
        void UpdataAccount(int id);
        void DeleteAccount(int id);
        void AddAcountToDb(Account account);
        List<AccountType> GetAcountTypeList(int id);
        void SetMainCurrancy(int id,string mainCurrancy);
        string GetMainCurrancy(int id);

    }
}
